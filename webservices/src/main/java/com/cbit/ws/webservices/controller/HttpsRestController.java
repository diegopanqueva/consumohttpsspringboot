package com.cbit.ws.webservices.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cbit.ws.webservices.model.Persona;

@RestController
@RequestMapping("/V1/nt-ms")
public class HttpsRestController {
	
	private final Logger log = LoggerFactory.getLogger(HttpsRestController.class);

	@GetMapping("/saludo")
	public ResponseEntity<?> saludar() {
		Map<String, Object> response = new HashMap<>();
		response.put("mensaje", "Hola Mundo!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PostMapping("/persona")
	public Persona registrarPersona(@RequestHeader Map<String, String> header, @RequestBody Persona persona) {
		Map<String, Object> response = new HashMap<>();
		String rquid = header.get("x-rquid");
		log.info(rquid);
		log.info(persona.getApellido());
		log.info(persona.getNombre());
		log.info(persona.getTipoDocumento());
		log.info(String.valueOf(persona.getNumeroDocumento()));
		response.put("mensaje", "Persona registrada satisfactoriamente!");
		//return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		return persona;
	}

}
