package com.cbit.ws.clientwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientwebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientwebserviceApplication.class, args);
	}

}
