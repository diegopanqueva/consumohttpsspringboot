package com.cbit.ws.clientwebservice.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cbit.ws.clientwebservice.model.Persona;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/V1")
public class ClienteController {

	private static final String URL = "https://localhost:9002/V1/nt-ms/persona";

	@Autowired
	private RestTemplate plantilla;

	@GetMapping("/saludar")
	public ResponseEntity<?> saludar() {
		Map<String, Object> response = new HashMap<>();
		response.put("mensaje", "Hola Mundo!");
		String respuesta = plantilla.getForObject("https://localhost:9002/V1/nt-ms/saludo", String.class);
		response.put("mensaje", respuesta);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PostMapping("/addperson")
	public ResponseEntity<?> registrarPersona(@RequestHeader Map<String, String> headersRecieve,
			@RequestBody Persona persona) {
		Map<String, Object> response = new HashMap<>();
		// create headers
		HttpHeaders headers = new HttpHeaders();
		// set content-type
		headers.setContentType(MediaType.APPLICATION_JSON);
		// set accept
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("x-rquid", "200517");

		// request body
		ObjectMapper obj = new ObjectMapper();
		// Convert my RequestBody for send to other ws
		@SuppressWarnings("unchecked")
		Map<String, Object> body = obj.convertValue(persona, Map.class);

		// build request
		HttpEntity<Map<String, Object>> sendPost = new HttpEntity<>(body, headers);
		ResponseEntity<Persona> person = plantilla.postForEntity(URL, sendPost, Persona.class);
		if (person.getStatusCode() == HttpStatus.OK) {
			response.put("Mensaje", "Persona creada satisfactoriamente");
			response.put("Persona", person.getBody());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}else {
			response.put("Mensaje", "Hubo un error, no se pudo registrar persona");
		}

		return new ResponseEntity<Map<String, Object>>(response, person.getStatusCode());
	}
}
