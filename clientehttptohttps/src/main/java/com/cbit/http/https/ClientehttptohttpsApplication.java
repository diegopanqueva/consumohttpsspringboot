package com.cbit.http.https;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientehttptohttpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientehttptohttpsApplication.class, args);
	}

}
