package com.cbit.http.https.controller;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cbit.http.https.model.Persona;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/cliente/consumo/V1")
public class ClientController {
	
	private static final String URL = "https://localhost:9002/V1/nt-ms/persona";

	private final Logger log = LoggerFactory.getLogger(ClientController.class);

	@PostMapping("/consumir")
	public ResponseEntity<?> consumirHttps(@RequestBody Persona persona, @RequestHeader Map<String, String> headersRecieve) {
		Map<String, Object> response = new HashMap<>();
		// create headers
		HttpHeaders headers = new HttpHeaders();
		// set content-type
		headers.setContentType(MediaType.APPLICATION_JSON);
		// set accept
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("x-rquid", "200517");

		// request body
		ObjectMapper obj = new ObjectMapper();
		// Convert my RequestBody for send to other ws
		@SuppressWarnings("unchecked")
		Map<String, Object> body = obj.convertValue(persona, Map.class);

		// build request
		HttpEntity<Map<String, Object>> sendPost = new HttpEntity<>(body, headers);
		ResponseEntity<Persona> person = getRestTemplate().postForEntity(URL, sendPost, Persona.class);
		if (person.getStatusCode() == HttpStatus.OK) {
			response.put("Mensaje", "Persona creada satisfactoriamente");
			response.put("Persona", person.getBody());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		} else {
			response.put("Mensaje", "Hubo un error, no se pudo registrar persona");
		}

		return new ResponseEntity<Map<String, Object>>(response, person.getStatusCode());
	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		KeyStore keyStore;
		HttpComponentsClientHttpRequestFactory requestFactory = null;
		try {
			keyStore = KeyStore.getInstance("jks");
			ClassPathResource classPathResource = new ClassPathResource("jks/truststore.jks");
			InputStream inputStream = classPathResource.getInputStream();
			keyStore.load(inputStream, "123456*".toCharArray());
			SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
					new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy())
							.loadKeyMaterial(keyStore, "123456*".toCharArray()).build(),
					NoopHostnameVerifier.INSTANCE);
			HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory)
					.setMaxConnTotal(Integer.valueOf(5)).setMaxConnPerRoute(Integer.valueOf(5)).build();

			requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			requestFactory.setReadTimeout(Integer.valueOf(10000));
			requestFactory.setConnectTimeout(Integer.valueOf(10000));

			restTemplate.setRequestFactory(requestFactory);

		} catch (Exception e) {
			log.error("No se pudo crer el restTemplate" + e);
		}
		return restTemplate;
	}
}
